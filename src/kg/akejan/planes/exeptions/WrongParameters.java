package kg.akejan.planes.exeptions;

public class WrongParameters extends Exception {
    public WrongParameters(String message) {
        super(message);
    }
}
