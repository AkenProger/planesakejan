package kg.akejan.planes.controllers;

import kg.akejan.planes.models.Plane;
import kg.akejan.planes.services.PlaneService;

public class PlaneController {
    private PlaneService planeService = PlaneService.INSTANCE;

    public void savePlan() {
        Plane plane = new Plane();
        planeService.savePlane(plane);

    }

}
