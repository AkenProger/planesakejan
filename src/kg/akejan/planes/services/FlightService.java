package kg.akejan.planes.services;

import kg.akejan.planes.exeptions.WrongParameters;
import kg.akejan.planes.models.Flight;
import kg.akejan.planes.services.impl.FlightServiceImpl;

public interface FlightService {

    FlightService INSTANSE_FLIGHT = new FlightServiceImpl();

    Flight createFlight(Flight flight) throws WrongParameters;

}
