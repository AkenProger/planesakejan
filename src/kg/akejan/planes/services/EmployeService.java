package kg.akejan.planes.services;

import kg.akejan.planes.enums.EmployeStatus;
import kg.akejan.planes.models.Employee;
import kg.akejan.planes.services.impl.EmployeServiceImpl;

public interface EmployeService {
    EmployeService INTANSE = new EmployeServiceImpl();

    boolean saveEmployee(Employee employee);

    Employee findEmployeById(Long id);

    boolean changeEmployeStatus(Long id, EmployeStatus newStatus);


}
