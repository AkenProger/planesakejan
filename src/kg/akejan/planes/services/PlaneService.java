package kg.akejan.planes.services;

import kg.akejan.planes.models.Plane;
import kg.akejan.planes.services.impl.PlaneServiceImpl;

import java.util.List;

public interface PlaneService {

    PlaneService INSTANCE = new PlaneServiceImpl();
    void savePlane(Plane plane);

    List<Plane> getPlanes();


}
