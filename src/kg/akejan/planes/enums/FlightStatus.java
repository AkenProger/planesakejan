package kg.akejan.planes.enums;

public enum FlightStatus {

    CANCELED,
    POSTPONED,
    EXPECTED,
    COMPLETED
}
